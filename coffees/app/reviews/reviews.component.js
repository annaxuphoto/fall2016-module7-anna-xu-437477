angular.
  module('myApp').
  component('reviews', {
    template:
    '<table>' +
      '<tr style="font-weight: bold">' +
        '<td>Reviewer: </td>' +
        '<td>Comment: </td>' +
        '<td>Rating: </td>' +
      '</tr>' +
  '<tr ng-repeat="review in $ctrl.reviews">' +
    '<td>{{ review.reviewer }} </td>' +
    '<td>{{ review.comment }} </td>' +
    '<td>{{ review.rating }} / 10</td>' +
  '</tr>' +
'</table>',
    controller: ['$routeParams',
      function ReviewDetailController($routeParams) {
        this.coffeeId = $routeParams.coffeeId;
        this.reviews = [];
        if(this.coffeeId == 1) {
          this.reviews =
            [
            {'rating': 3,
             'comment': "Could've been crispier",
             'reviewer': "Chris P. Bacon"
            }
            ];
        }
        if(this.coffeeId == 2) {
          this.reviews =
          [
       {'rating': 10,
       'comment': 'What everyone should drink in the morning!',
       'reviewer': 'Earl Lee Riser'
       },
       {'rating': 10,
       'comment': 'A genius of everything coffee',
       'reviewer': 'Bob'
       }
       ];
        }
        if(this.coffeeId == 3) {
          this.reviews =
          [
       {'rating': 1,
       'comment': " a 'latte' yuckiness.",
       'reviewer': 'Tim Burr'
       },
       {'rating': 1,
       'comment': 'Is this even coffee?',
       'reviewer': 'Sue Flay'
       },
        {'rating': 1,
       'comment': 'The grossest thing I have ever had.',
       'reviewer': 'Myles Long'
       },
        {'rating': 5,
       'comment': 'I dont know what the fuss is about, i dont think its too bad!',
       'reviewer': 'Sara Bellum'
       }
       ];
        }
        if(this.coffeeId == 4) {
          this.reviews =
          [
       {'rating': 10,
       'comment': 'If I could rate it higher, I would!',
       'reviewer': 'Justin Case'
       },
       {'rating': 10,
       'comment': 'He does it again!',
       'reviewer': 'Eileen Dover'
       }
       ];
        }
      }
    ]
  });