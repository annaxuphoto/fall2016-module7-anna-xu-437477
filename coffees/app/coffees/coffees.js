'use strict';

angular.module('myApp.coffees', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/coffees', {
    templateUrl: 'coffees/coffees.html',
    controller: 'CoffeesCtrl'
  });
}])

.controller('CoffeesCtrl', [function() {

}]);